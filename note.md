# dos 指令
> 查詢占用的Port 
-- Window => netstat  -aon|findstr 3306
> 查詢目前執行的程式
-- Window => tasklist

# mysql
> Windows
1. 開啟 cmd 命令列工具，以管理員身份執行
2. 執行命令：cd….切換到 mysql 根目錄下
3. 執行命令：mysqld --initialize --user=mysql --console 該命令執行中的最後，mysql會分配一個 temporary password，記住：CZX3ge1kuz)L
4. 執行命令：mysqld --install MySQL
5. 安裝完成
6. net start mysql 啟動服務
7. net stop mysql 停止服務
8. sc delete MySQL 刪除服務（按需使用）
9. 使用初始密碼登入後，使用 set password for root@localhost=password(‘新密碼’) 來更改密碼

#phpMyAdmin
download => https://www.phpmyadmin.net
教學 => https://t4cgbb.space/2017/09/10/phpmyadmin%E9%85%8D%E7%BD%AE/

# docker
## 使用 dockerFiles 做 laravel
### PS apt-get install php7.3-mysql
1. 於放dockerFile 的根目錄執行 docker-compose up -d 或 docker-compose up -d --force-recreate --build
1. docker-compose build
1. 進入終端機 docker exec -it [containerID] bash 
1. 進入專案目錄 /var/www/html/
1. 更名 mv .env.example .env
1. php artisan key:generate
1. 修改 .env mysql 連線資訊
1. 安裝 mysql 要使用的資料庫和設定使用者
1. 安裝 composer update
1. 設定資夾權限 chmod -R 777 /var/www/html/storage/
1. 設定域名 chown -R www-data:www-data /var/www/html/
1. service apache2 restart
1. a2enmod rewrite
1. php artisan make:auth
1. php artisan migrate
1. php artisan tinker
1. 設定根目錄 cp server/000-default.conf /etc/apache2/sites-enabled/000-default.conf

## 安裝有樣式的 debug 工具
apt-get install php-xdebug

## install mysql mysqlAdmin
apt-get install mysql-server phpmyadmin php7.3-mysql php7.3-curl php7.3-odbc php7.3-sqlite3 -y

> mysqlAdmin 配置
1. 修改別名(預設: http://phpmyadmin) 
vim /etc/apache2/conf-enabled/phpmyadmin.conf 
Alias </your-alias-folder> /usr/share/phpmyadmin
:wq!
2. 配置 apache 與 myadmin 的連線
vim /etc/apache2/apache2.conf
Include /etc/phpmyadmin/apache.conf
:wq!
3. 將 myadmin 執行檔置放網頁目錄下
ln -s /user/share/phpmyadmin /var/www/html/phpmyadmin

> mysaqlAdmin 錯誤排除
1. count(): Parameter must be an array or an object that implements Countable
vim /usr/share/phpmyadmin/libraries/plugin_interface.lib.php
551行 > if ($options != null && count($options) > 0 ) 變 if ($options != null)
vim /usr/share/phpmyadmin/libraries/sql.lib.php
613行 > (count($analyzed_sql_results['select_expr'] == 1) 變 ((count($analyzed_sql_results['select_expr']) == 1)

## 安裝資料庫
CREATE DATABASE `jinnik` DEFAULT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
<!-- CREATE USER 'jinnik'@'localhost' IDENTIFIED BY '123456789'; -->
CREATE USER 'jinnik'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456789';
GRANT ALL PRIVILEGES ON *.* TO 'jinnik'@'localhost';
FLUSH PRIVILEGES;