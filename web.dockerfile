FROM ubuntu:18.04

#env 環境變數
ENV DEBIAN_FRONTEND=noninteractive

# 舊指令
# RUN apt-get -y update && apt-get -y upgrade && apt-get install software-properties-common -y && add-apt-repository ppa:ondrej/php -y && add-apt-repository ppa:certbot/certbot -y 
# RUN apt-get update -y && apt-get -y install php7.1 php7.1-gd libapache2-mod-php7.1 php7.1-mysql php7.1-zip php7.1-mbstring php7.1-xml php7.1-curl zip unzip composer mysql-server python-certbot-apache

# 以下為新指令
RUN apt-get -y update && apt-get -y upgrade \
&& apt-get install software-properties-common -y \
&& add-apt-repository ppa:ondrej/php -y && add-apt-repository ppa:certbot/certbot -y \
&& apt-get update -y && apt-get -y install php7.3 php7.3-gd libapache2-mod-php7.3 php7.3-mysql php7.3-zip php7.3-mbstring php7.3-xml php7.3-curl php7.3-odbc php7.3-sqlite3 \
&& apt-get install -y sudo vim mysql-server phpmyadmin zip unzip composer mysql-server python-certbot-apache

RUN usermod -d /var/lib/mysql/ mysql
RUN ln -s /var/lib/mysql/mysql.sock /tmp/mysql.sock
RUN chown -R mysql:mysql /var/lib/mysql

COPY server/000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN server mysql apache2 restart
RUN a2enmod rewrite && apt-get -y update && apt-get -y upgrade